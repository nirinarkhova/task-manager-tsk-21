package ru.nirinarkhova.tm.service;

import ru.nirinarkhova.tm.api.service.IAuthService;
import ru.nirinarkhova.tm.api.service.IUserService;
import ru.nirinarkhova.tm.exception.empty.EmptyLoginException;
import ru.nirinarkhova.tm.exception.empty.EmptyPasswordException;
import ru.nirinarkhova.tm.exception.system.AccessDeniedException;
import ru.nirinarkhova.tm.model.User;
import ru.nirinarkhova.tm.util.HashUtil;

import java.util.Optional;

public class AuthService implements IAuthService {

    private final IUserService userService;

    private String userId;

    public AuthService(IUserService userService) {
        this.userService = userService;
    }

    @Override
    public Optional<User> getUser() {
        final String userId = getUserId();
        return userService.findById(userId);
    }

    @Override
    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public void login(String login, String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final Optional<User> user = userService.findByLogin(login);
        if (!user.isPresent()) throw new AccessDeniedException();
        final String hash = HashUtil.salt(password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.get().getPasswordHash())) throw new AccessDeniedException();
        userId = user.get().getId();
    }

    @Override
    public void register(String login, String password, String email) {
        userService.create(login, password, email);
    }


}

