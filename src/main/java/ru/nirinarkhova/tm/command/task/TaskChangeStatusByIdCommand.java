package ru.nirinarkhova.tm.command.task;

import ru.nirinarkhova.tm.command.AbstractTaskCommand;
import ru.nirinarkhova.tm.enumerated.Status;
import ru.nirinarkhova.tm.exception.entity.TaskNotFoundException;
import ru.nirinarkhova.tm.model.Task;
import ru.nirinarkhova.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Optional;

public class TaskChangeStatusByIdCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-change-status-by-id";
    }

    @Override
    public String description() {
        return "change task status by task id.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CHANGE TASK STATUS]");
        System.out.println("[ENTER TASK ID:]");
        final String id = TerminalUtil.nextLine();
        System.out.println("[ENTER TASK STATUS:]");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Optional<Task> task = serviceLocator.getTaskService().findById(userId, id);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        final Optional<Task> taskStatusUpdate = serviceLocator.getTaskService().changeStatusById(userId, id, status);
        Optional.ofNullable(taskStatusUpdate).orElseThrow(TaskNotFoundException::new);
    }

}

