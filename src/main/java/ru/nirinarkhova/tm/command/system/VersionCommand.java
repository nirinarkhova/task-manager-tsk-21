package ru.nirinarkhova.tm.command.system;

import ru.nirinarkhova.tm.command.AbstractCommand;

public class VersionCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-v";
    }

    @Override
    public String name() {
        return "version";
    }

    @Override
    public String description() {
        return "show application version.";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION: 1.0.0]");
    }

}

