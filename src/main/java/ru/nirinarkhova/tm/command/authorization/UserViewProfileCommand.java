package ru.nirinarkhova.tm.command.authorization;

import ru.nirinarkhova.tm.command.AbstractCommand;
import ru.nirinarkhova.tm.model.User;

import java.util.Optional;

public class UserViewProfileCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "view-profile";
    }

    @Override
    public String description() {
        return "show your profile information.";
    }

    @Override
    public void execute() {
        final Optional<User> user = serviceLocator.getAuthService().getUser();
        System.out.println("[VIEW PROFILE]");
        System.out.println("LOGIN: " + user.get().getLogin());
        System.out.println("EMAIL: " + user.get().getEmail());
        System.out.println("FIRST NAME: " + user.get().getFirstName());
        System.out.println("LAST NAME: " + user.get().getLastName());
        System.out.println("MIDDLE NAME: " + user.get().getMiddleName());
    }

}

