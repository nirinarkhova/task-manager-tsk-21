package ru.nirinarkhova.tm.command.authorization;

import ru.nirinarkhova.tm.command.AbstractCommand;

public class UserLogOutCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "user-logout";
    }

    @Override
    public String description() {
        return "log you out of the system.";
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        serviceLocator.getAuthService().logout();
    }

}

