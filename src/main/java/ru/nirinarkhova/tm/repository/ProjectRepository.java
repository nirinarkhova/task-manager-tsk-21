package ru.nirinarkhova.tm.repository;

import ru.nirinarkhova.tm.api.repository.IProjectRepository;
import ru.nirinarkhova.tm.model.Project;

public class ProjectRepository extends AbstractBusinessRepository<Project> implements IProjectRepository {

}

