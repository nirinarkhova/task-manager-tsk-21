package ru.nirinarkhova.tm.api.repository;

import ru.nirinarkhova.tm.api.IBusinessRepository;
import ru.nirinarkhova.tm.model.Task;

import java.util.List;
import java.util.Optional;

public interface ITaskRepository extends IBusinessRepository<Task> {

    List<Task> findAllByProjectId(String userId, String projectId);

    void removeAllByProjectId(String userId, String projectId);

    Optional<Task> bindTaskToProject(String userId, String taskId, String projectId);

    Optional<Task> unbindTaskFromProject(String userId, String projectId);

}

