package ru.nirinarkhova.tm.api.entity;

import ru.nirinarkhova.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
