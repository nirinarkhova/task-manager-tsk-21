package ru.nirinarkhova.tm.api.service;

import ru.nirinarkhova.tm.model.Project;
import ru.nirinarkhova.tm.model.Task;

import java.util.List;
import java.util.Optional;

public interface IProjectTaskService {

    List<Task> findTaskByProjectId(String userId, String projectId);

    Optional<Task> bindTaskByProject(String userId, String taskId, String projectId);

    Optional<Task> unbindTaskFromProject(String userId, String taskId);

     Project removeProjectById(String userId, String projectId);

}
