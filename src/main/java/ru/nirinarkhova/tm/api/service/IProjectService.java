package ru.nirinarkhova.tm.api.service;

import ru.nirinarkhova.tm.api.IBusinessService;
import ru.nirinarkhova.tm.model.Project;

public interface IProjectService extends IBusinessService<Project> {

    Project add(String userId, String name, String description);

}

